package com.example.rabbitspringsubmissionpublish;


import com.example.rabbitspringsubmissionpublish.Repository.UserRepository;
import com.example.rabbitspringsubmissionpublish.configuration.rabbitconfiguration;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

import java.util.*;

@Controller
public class EmployeeController {

    private final Logger logger= LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    public RabbitTemplate rabbitTemplate;
    @Autowired
    public UserRepository userRepository;

    @GetMapping("/")
    public String login(){

        return "loginform";
    }

    @PostMapping("/login_validate")
    public String validate_login(@RequestParam("username") String username,
                                 @RequestParam("password") String password,
                                 ModelMap map){
        logger.debug("<< Received Login details : "+username);
        List<User> user=userRepository.findByUsername(username);
        if (user.size()==0){
            logger.debug(">> No User Available with username : "+username);
            return "loginfailed";
        }
        else {
            if (user.get(0).getPassword().equals(password)) {
                logger.debug(">> Logged IN Successfully........");
                System.out.println("Logged In as User "+username);
                return "Submission";
            } else {
                logger.debug(">> Wrong Password");
                return "loginfailed";
            }
        }
    }

    @GetMapping("/register_page")
    public String register_a(){
        return "register";
    }

    @PostMapping("/register")
    public String Addtomydb(@RequestParam("username") String username,@RequestParam("password") String password,ModelMap map){

        User user=new User();
        Random rnd=new Random();
        List<User> allusers=userRepository.findAll();
        int sizeofusers=allusers.size();
        user.setId(sizeofusers+1);
        user.setUsername(username);
        user.setPassword(password);
        userRepository.save(user);
        logger.debug(">> Registered with username : "+username);
        return "loginform";
    }

    @GetMapping("/submit")
    public String GetSubmissionform(){
        return "Submission";
    }
    @PostMapping("Add")
    public String SubmitForm(@RequestParam("id") int id,
                             @RequestParam("EmpName") String EmpName,
                             @RequestParam("EmpDept") String EmpDept,
                             @RequestParam("EmpBGrp") String EmpBGrp,
                             ModelMap map){
        map.put("id",id);
        map.put("EmpName",EmpName);
        map.put("EmpDept",EmpDept);
        map.put("EmpBGrp",EmpBGrp);
        Employee emp=new Employee();
        emp.setId(id);
        emp.setEmpName(EmpName);
        emp.setEmpDept(EmpDept);
        emp.setEmpBGrp(EmpBGrp);
        System.out.println(emp);
        rabbitTemplate.convertAndSend(rabbitconfiguration.EXCHANGE,rabbitconfiguration.ROUTINGKEY,emp);
        return "Addedform";
    }
}
